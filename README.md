Subreddit analysis
==================

What does it do?
----------------

The script examines the most "hotest" posts in a specified subreddit and uses this to find which other subreddits the contributors post to the most. 

Usage
-----

1. Set up `python3` with `praw` installed.
2. Let the script log into a reddit account by creating a file called `auth` containing 2 lines:

```text
username
password
```

3. Run the tool:

```bash
./sra.py --auth auth sub_to_analyse
```

Substitute the name of a subreddit to examine. If you called the auth file `auth` and it is in the current directory you can omit the `--auth auth`. Run with `-h` to see other options.

Authors
-------

Code originally taken from https://www.reddit.com/r/SubredditAnalysis/comments/26862c/meta_i_improved_the_code_and_also_accept where it was developed by SirNeon and friendlyprogrammer

Updated by a Greyly Granulated Gabon
